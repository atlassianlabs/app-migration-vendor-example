package com.vendor.cloudapp.local;

import com.atlassian.connect.spring.IgnoreJwt;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * This is no longer the preferred way to register webhook
 *
 * @see <a href="https://developer.atlassian.com/platform/app-migration/events/#app-migration-webhook-in-connect-descriptor">App Migration Webhook In Connect Descriptor</a>
 * for a more reliable way to register webhooks
 */
@IgnoreJwt
@RestController
@RequestMapping("webhook")
public class WebhookRegistrationController extends BaseController {

    @PutMapping
    public ResponseEntity<Void> putWebHook(@RequestBody NotificationEndpoints webhook) {
        return (call("/webhook", HttpMethod.PUT, webhook, Void.class));
    }

    @GetMapping
    public ResponseEntity<String> getWebHook() {
        return (call("/webhook", HttpMethod.GET, null, String.class));
    }
}
