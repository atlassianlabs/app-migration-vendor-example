package com.vendor.cloudapp.local;

import com.atlassian.connect.spring.IgnoreJwt;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.apache.commons.lang3.ObjectUtils.isNotEmpty;

@IgnoreJwt
@RestController
@RequestMapping("migration")
public class MappingMigrationController extends BaseController {

    @RequestMapping(value = "/transfer/recent", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<List> getRecentTransfers() {
        return call("/transfer/recent", HttpMethod.GET, null, List.class);
    }

    @RequestMapping(value = "/mapping/{transferId}/page", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<String> getMappingsForTransferId(@PathVariable String transferId, @RequestParam String namespace, @RequestParam(required = false) String lastEntity, @RequestParam(required = false) String pageSize) {
        return (call("/mapping/" + transferId + "/page?" + paginationQueryParamsNameSpace(lastEntity, pageSize, namespace), HttpMethod.GET, null, String.class));
    }

    @RequestMapping(value = "/mapping/{transferId}/find", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<Map> getMappingsForTransferId(@PathVariable String transferId, @RequestParam String namespace, @RequestBody Set<String> ids) {
        return (call("/mapping/" + transferId + "/find?namespace=" + namespace, HttpMethod.POST, ids, Map.class));
    }

    @RequestMapping(value = "/feedback/{transferId}", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public ResponseEntity<Void> postFeedbackForTransferId(@PathVariable String transferId, @RequestBody CloudFeedback cloudFeedback) {
        return (call("/feedback/" + transferId, HttpMethod.POST, cloudFeedback, Void.class));
    }

    @RequestMapping(value = "/data/{fileId}", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<String> getS3URLForFileId(@PathVariable String fileId) {
        return (call("/data/" + fileId, HttpMethod.GET, null, String.class));
    }

    @RequestMapping(value = "/data/{transferId}/all", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<String> getAllFilesForTransferId(@PathVariable String transferId) {
        return (call("/data/" + transferId + "/all", HttpMethod.GET, null, String.class));
    }

    @RequestMapping(value = "/progress/{transferId}", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public void postStatusForTransferId(@PathVariable String transferId, @RequestBody ProgressEndpointDto progressEndpointDto) {
        atlassianConnectClient.updateProgress(transferId, progressEndpointDto);
    }

    @RequestMapping(value = "/container/{transferId}/page", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<String> getContainersForTransferId(@PathVariable String transferId, @RequestParam String containerType, @RequestParam(required = false) String lastEntity, @RequestParam(required = false) String pageSize) {
        return (call("/container/" + transferId + "/page?" + paginationQueryParamsContainerType(lastEntity, pageSize, containerType), HttpMethod.GET, null, String.class));
    }

    @RequestMapping(value = "/error/{transferId}", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<String> fetchServerSideErrorDetails(@PathVariable String transferId) {
        return (call("/error/" + transferId, HttpMethod.GET, null, String.class));
    }

    private String paginationQueryParamsNameSpace(String lastEntity, String pageSize, String namespace) {
        String result = "";
        result += getPaginationQueryParam(lastEntity, pageSize);
        if (isNotEmpty(namespace)) result += "&namespace=" + namespace;
        return result.replaceFirst("&", "");
    }

    private String paginationQueryParamsContainerType(String lastEntity, String pageSize, String containerType) {
        String result = "";
        result += getPaginationQueryParam(lastEntity, pageSize);
        if (isNotEmpty(containerType)) result += "&containerType=" + containerType;
        return result.replaceFirst("&", "");
    }

    private String getPaginationQueryParam(String lastEntity, String pageSize) {
        String result = "";
        if (isNotEmpty(lastEntity)) result += "&lastEntity=" + lastEntity;
        if (isNotEmpty(pageSize)) result += "&pageSize=" + pageSize;
        return result;
    }
}
