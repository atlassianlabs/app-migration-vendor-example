package com.vendor.cloudapp.local;

import java.util.Map;

public class CloudFeedback{
    private Map<String, String> details;

    public Map<String, String> getDetails() {
        return details;
    }

    public void setDetails(Map<String, String> details) {
        this.details = details;
    }
}
