package com.vendor.cloudapp.local;

public enum Status {
    IN_PROGRESS,
    SUCCESS,
    FAILED,
    INCOMPLETE
}

