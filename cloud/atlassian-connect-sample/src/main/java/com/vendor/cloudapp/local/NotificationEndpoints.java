package com.vendor.cloudapp.local;

import java.util.HashSet;
import java.util.Set;

public class NotificationEndpoints {

    private Set<String> endpoints = new HashSet();

    public Set<String> getEndpoints() {
        return endpoints;
    }

    public void setEndpoints(Set<String> endpoints) {
        this.endpoints = endpoints;
    }
}
