# Atlassian Connect Sample  
  
To run this project you will need some prerequisites:  
  
 - A Confluence Cloud Site.   
 - ngrok (https://ngrok.com/)
 
## Step-by-step:

#### 1 - Run Ngrok

    ngrok http 9090

save your Ngrok endpoint, e.g. https://570a32f41.ngrok.io.

#### 2 - Update your settings

Go to `application.yml` and set your environment variables using the example below:
```yaml
cloud.site.url: https://your-cloud-site.atlassian.net/
atlassian.product: confluence
addon:
  key: my-cloud-app-key
  base-url: https://570a32f41.ngrok.io
```

#### 3 - Run the project

```shell
mvn spring-boot:run
```
    
The project will expose the Atlassian Connect file endpoint: 
	https://570a32f41.ngrok.io/atlassian-connect.json ( you will use this endpoint to install the app on the cloud ).

#### 3 - Upload the App on Cloud Site

This step will let you upload your app to the cloud-site. This means your app's endpoints are registered with the cloud-site. Therefore, cloud-site could send request to your app in case of product related events -  (e.g. page updates for Confluece, and Issue update for JIRA).

##### On Confluence

Go to:  `Settings -> Manage Apps -> click on Settings -> Enable development mode`
On the Manage Apps page: click on Upload App -> use your Atlassian Connect address ( https://570a32f41.ngrok.io/atlassian-connect.json  )

***Obs***: there is a file inside the project: *addon-db.script*. Clear every line that looks like " *INSERT INTO ATLASSIAN_HOST VALUES('6319dcf0-70f6-38c9-930d-42ea959c9f86 ...."*, otherwise the app will use this token to validate the install, but this could be an old token. If you stopped Ngrok for any reason then you should remove this line and run the install process again.

##### On JIRA

Similar to Confluence step.

#### 4 - App Migration Endpoints

In the sample app(src/main/java/com/vendor/cloudapp/local), there are following controllers available to show App Migration related Endpoints.
 - WebhookRegistrationController: This shows how to GET/PUT webhooks related to App Migration events. For DELETE - use PUT /webhook with empty list of notification endpoints.
 - MappingMigrationController: This shows the following types of requests:
    - /migration: Returns a list of migrations available for the provided cloudAppKey
    - /migration/mapping/{transferId}/page?namespace={val}: Returns mappings for a given namespace and transferId, using pagination. Check MappingMigrationController handler for the usage of lastEntity, and pageSize query parameters.
    - /migration/data/{fileId}: Returns an object containing a signed url to retrieve app data. Note this will not work at present as Server side library doesn't allow to upload any files.
    - /migration/feedback/{transferId} : This is to update status of migration back to App Migration Services. A map containing status/errormessage etc
    - /migration/transfer/recent: Returns a list of latest active transfers (up to 100) for the cloud app that originated the request, already updated to the last version which doesn't contains the field Containers.
    - /migration/container/page?containerType={val}: Returns containers for a given transferId using pagination. Check MappingMigrationController handler for the usage of lastEntity, and pageSize query parameters. Example ContainerTypes are JiraProject, ConfluenceSpace, and Site. 
    
 One can send request to http://localhost:9090 to those controllers and those controllers will send the request to Cloud-site. This is just to show how the communication should be done from your cloud app to cloud-site.

#### 5 - App Migration Related Events

After registering webhooks, your cloud app will be notified for the migration related events on the webhooks you registered on step 5. The structure of the event is shown by MigrationEvent class.

#### 6 - [Optional] Customising Webhooks For Products - Confluece/JIRA

You can choose which events will be sent to your app. 
 - Check the details [https://developer.atlassian.com/cloud/confluence/modules/webhook/](https://developer.atlassian.com/cloud/confluence/modules/webhook/)
 - Update the atlassian-connect.json file on the project (a reinstall will be necessary)
 - Remove the lines from addon-db.script file

# References

Check some links from the official public doc about Atlassian Connect and Integrating with Conf Cloud.
[https://connect-inspector.prod.public.atl-paas.net/page/start](https://connect-inspector.prod.public.atl-paas.net/page/start)
[https://ngrok.com/docs#inspect](https://ngrok.com/docs#inspect)
[https://developer.atlassian.com/cloud/confluence/modules/webhook/](https://developer.atlassian.com/cloud/confluence/modules/webhook/)
[https://developer.atlassian.com/cloud/confluence/getting-started/](https://developer.atlassian.com/cloud/confluence/getting-started/)
[https://developer.atlassian.com/cloud/confluence/confluence-connect-patterns/](https://developer.atlassian.com/cloud/confluence/confluence-connect-patterns/)
[https://developer.atlassian.com/cloud/confluence/introduction-to-confluence-connect/](https://developer.atlassian.com/cloud/confluence/introduction-to-confluence-connect/)

# License

Copyright (c) 2020 Atlassian and others.
Apache 2.0 licensed, see [LICENSE.txt](https://bitbucket.org/atlassianlabs/app-migration-vendor-example/src/master/LICENSE.txt) file.

