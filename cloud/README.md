App Migration - Cloud App Examples
 ==============
 
 A collection of cloud apps (connect app) that shows how to interact with the App Migration platform. 
 
- atlassian-connect-sample: Example of how to use app migration endpoints and get notifications.
- atlassian-connect-JiraWorkflowRule-sample: Example of how to migrate Jira workflow rules using app migration endpoints.
- atlassian-connect-jiraCustomField-sample: Example of how to migrate Jira custom fields using app migration endpoints.
- atlassian-harmonised-app-sample: Example of how to register app migration webhook endpoint and use app migration endpoints in a harmonised app.

 Full documentation of atlassian connect is always available at:

 https://developer.atlassian.com/cloud/jira/platform/getting-started-with-connect/ 
