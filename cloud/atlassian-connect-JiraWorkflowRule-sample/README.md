# Atlassian Connect Workflow Sample  
  
To run this project you will need some prerequisites:  
  
 - A Jira Cloud Site.   
 - ngrok (https://ngrok.com/)

## Step-by-step:

### 1 - Run Ngrok

    ngrok http 9090

save your Ngrok endpoint, ex: https://570a32f41.ngrok.io.

### 2 - Update your settings

Go to `appllicaiton.yml` and set your environment. Specifically the variables given as example below:
```yaml
cloud.site.url: https://your-cloud-site.atlassian.net/
atlassian.product: jira
addon:
  key: my-cloud-app-key
  base-url: https://570a32f41.ngrok.io
```

### 3 - Run the project

```shell
mvn spring-boot:run
```
    
The project will expose the Atlassian Connect file endpoint: 
	https://570a32f41.ngrok.io/atlassian-connect.json ( you will use this endpoint to install the app on the cloud ).

### 4 - Upload the App on Cloud Site

This step will let you upload your app to the cloud-site. This means your app's endpoints are registered with the cloud-site. Therefore, cloud-site could send request to your app in case of product related events -  (e.g. Issue update for JIRA).

#### On JIRA

Go to:  `Settings -> Manage Apps -> click on Settings -> Enable development mode`
On the Manage Apps page: click on Upload App -> use your Atlassian Connect address ( https://570a32f41.ngrok.io/atlassian-connect.json  )

***Obs***: there is a file inside the project: *addon-db.script*. Clear every line that looks like " *INSERT INTO ATLASSIAN_HOST VALUES('6319dcf0-70f6-38c9-930d-42ea959c9f86 ...."*, otherwise the app will use this token to validate the install, but this could be an old token. If you stopped Ngrok for any reason then you should remove this line and run the install process again.

### 5 - App Migration Endpoints

In the sample app(src/main/java/com/vendor/cloudapp/local/workflow), you will find the following controller endpoint.
 - LocalWorkflowController: a /POST API used to request details from the migrated workflow rules.

### 6 - Retrieving migrated workflowRules details.

Use the existing GET mapping API to retrieve the workflowRules ids under the `jira/classic:workflowRule` namespace.

Use the new available API to retrieve the workflowRules details, more details at `com.vendor.cloudapp.local.workflow.LocalWorkflowController`.

Use either [https://developer.atlassian.com/cloud/jira/platform/rest/v3/api-group-workflow-transition-rules/#api-rest-api-3-workflow-rule-config-put](https://developer.atlassian.com/cloud/jira/platform/rest/v3/api-group-workflow-transition-rules/#api-rest-api-3-workflow-rule-config-put) or
 [https://developer.atlassian.com/cloud/jira/platform/rest/v3/api-group-workflow-transition-rules/#api-rest-api-3-workflow-rule-config-delete-put](https://developer.atlassian.com/cloud/jira/platform/rest/v3/api-group-workflow-transition-rules/#api-rest-api-3-workflow-rule-config-delete-put) to fix the migrated workflow rules.

## References

Check some links from the official public doc about Atlassian Connect and Integrating with Jira Cloud.
[https://connect-inspector.prod.public.atl-paas.net/page/start](https://connect-inspector.prod.public.atl-paas.net/page/start)
[https://ngrok.com/docs#inspect](https://ngrok.com/docs#inspect)
[https://developer.atlassian.com/cloud/jira/platform/webhooks/](https://developer.atlassian.com/cloud/jira/platform/webhooks/)
[https://developer.atlassian.com/cloud/jira/software/](https://developer.atlassian.com/cloud/jira/software/)
[https://developer.atlassian.com/cloud/jira/software/getting-started/](https://developer.atlassian.com/cloud/jira/software/getting-started/)
[https://developer.atlassian.com/cloud/jira/software/patterns-and-examples/](https://developer.atlassian.com/cloud/jira/software/patterns-and-examples/)

## License

Copyright (c) 2020 Atlassian and others.
Apache 2.0 licensed, see [LICENSE.txt](https://bitbucket.org/atlassianlabs/app-migration-vendor-example/src/master/LICENSE.txt) file.

