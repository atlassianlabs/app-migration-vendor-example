package com.vendor.cloudapp.local.workflow;

import com.google.gson.Gson;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

@Controller
public class WorkflowController {

    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public String create() {
        return "create";
    }

    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public String edit(@RequestParam("id") String validatorId,
                       @RequestParam("config") String config,
                       Map<String, Object> model) {
        model.put("id", validatorId);
        model.put("config", config);
        model.put("regexp", getRegexp(config));
        return "edit";
    }

    @RequestMapping(value = "/view-condition", method = RequestMethod.GET)
    public String viewCondition(@RequestParam("id") String validatorId, @RequestParam("config") String config, Map<String, Object> model) {
        return renderView("condition", validatorId, config, model);
    }

    @RequestMapping(value = "/view-validator", method = RequestMethod.GET)
    public String viewValidator(@RequestParam("id") String validatorId, @RequestParam("config") String config, Map<String, Object> model) {
        return renderView("validator", validatorId, config, model);
    }

    private String renderView(String name, String id, String config, Map<String, Object> model) {
        model.put("id", id);
        model.put("config", config);
        model.put("regexp", getRegexp(config));
        return "view-" + name;
    }

    private Object getRegexp(String config) {
        Map configMap = new Gson().fromJson(config, Map.class);
        return configMap != null ? configMap.get("regexp") : "";
    }
}
