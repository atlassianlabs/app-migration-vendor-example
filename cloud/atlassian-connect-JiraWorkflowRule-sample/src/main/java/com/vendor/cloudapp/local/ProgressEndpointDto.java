package com.vendor.cloudapp.local;

public record ProgressEndpointDto(Status status, int percent, String message) {
}