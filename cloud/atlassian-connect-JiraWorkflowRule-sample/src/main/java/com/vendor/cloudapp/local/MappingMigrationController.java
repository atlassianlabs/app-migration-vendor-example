package com.vendor.cloudapp.local;

import com.atlassian.connect.spring.IgnoreJwt;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.apache.commons.lang3.ObjectUtils.isNotEmpty;

@IgnoreJwt
@RestController
@RequestMapping("migration")
public class MappingMigrationController extends BaseController {

    @RequestMapping(value = "/mapping/{transferId}/page", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<String> getMappingsForTransferId(@PathVariable String transferId, @RequestParam String namespace, @RequestParam(required = false) String lastEntity, @RequestParam(required = false) String pageSize) {
        return call("/mapping/" + transferId + "/page?" + paginationQueryParamsNameSpace(lastEntity, pageSize, namespace), HttpMethod.GET, null, String.class);
    }

    @RequestMapping(value = "/progress/{transferId}", method = RequestMethod.POST, consumes = "application/json", produces = "application/json")
    public void postStatusForTransferId(@PathVariable String transferId, @RequestBody ProgressEndpointDto progressEndpointDto) {
        atlassianConnectClient.updateProgress(transferId, progressEndpointDto);
    }

    private String paginationQueryParamsNameSpace(String lastEntity, String pageSize, String namespace) {
        String result = "";
        result += getPaginationQueryParam(lastEntity, pageSize);
        if (isNotEmpty(namespace)) result += "&namespace=" + namespace;
        return result.replaceFirst("&", "");
    }

    private String getPaginationQueryParam(String lastEntity, String pageSize) {
        String result = "";
        if (isNotEmpty(lastEntity)) result += "&lastEntity=" + lastEntity;
        if (isNotEmpty(pageSize)) result += "&pageSize=" + pageSize;
        return result;
    }
}   
