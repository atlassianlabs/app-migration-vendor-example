package com.vendor.cloudapp.local;

import com.vendor.cloudapp.client.AtlassianConnectClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpStatusCodeException;

import java.util.Map;

public class BaseController {

    @Autowired
    protected AtlassianConnectClient atlassianConnectClient;


    protected <R, B> ResponseEntity call(String url, HttpMethod method, B jsonBody, Class<R> responseType) {
        try {
            return ResponseEntity.ok(atlassianConnectClient.call(url, method, jsonBody, responseType));
        } catch (HttpStatusCodeException e) {
            return ResponseEntity.status(e.getStatusCode()).headers(e.getResponseHeaders()).body(e.getResponseBodyAsString());
        }
    }

    protected <R, B> ResponseEntity callWithAdditionalHeaders(String url, HttpMethod method, B jsonBody, Class<R> responseType, Map<String, String> additionalHeaders) {
        try {
            return ResponseEntity.ok(atlassianConnectClient.callWithAdditionalHeaders(url, method, jsonBody, responseType, additionalHeaders));
        } catch (HttpStatusCodeException e) {
            return ResponseEntity.status(e.getStatusCode()).headers(e.getResponseHeaders()).body(e.getResponseBodyAsString());
        }
    }
}

