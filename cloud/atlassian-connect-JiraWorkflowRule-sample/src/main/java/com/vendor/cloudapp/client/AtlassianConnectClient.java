package com.vendor.cloudapp.client;

import com.atlassian.connect.spring.AtlassianHostRestClients;
import com.vendor.cloudapp.local.ProgressEndpointDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

import static java.util.Collections.singletonList;
import static org.springframework.http.MediaType.APPLICATION_JSON;

@Component
public class AtlassianConnectClient {

    public static final String ATLASSIAN_CONNECT_MIGRATION_URL = "/rest/atlassian-connect/1/migration";

    private final AtlassianHostRestClients atlassianHostRestClients;

    @Value("${cloud.site.url}")
    private String cloudSiteUrl;

    public AtlassianConnectClient(AtlassianHostRestClients atlassianHostRestClients) {
        this.atlassianHostRestClients = atlassianHostRestClients;
    }

    public void updateProgress(String transferId, ProgressEndpointDto progressEndpointDto) {
        call("/progress/" + transferId, HttpMethod.POST, progressEndpointDto, Void.class);
    }

    public <R, B> R call(String relativeUrl, HttpMethod method, B jsonBody, Class<R> responseType) {
        return callConnect(relativeUrl, method, jsonBody, responseType, new HashMap<>());
    }

    public <R, B> R callWithAdditionalHeaders(String relativeUrl, HttpMethod method, B jsonBody, Class<R> responseType, Map<String, String> additionalHeaders) {
        return callConnect(relativeUrl, method, jsonBody, responseType, additionalHeaders);
    }

    private <R, B> R callConnect(String relativeUrl, HttpMethod method, B jsonBody, Class<R> responseType, Map<String, String> additionalHeaders) {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(singletonList(APPLICATION_JSON));
        headers.setContentType(APPLICATION_JSON);
        for (Map.Entry entry : additionalHeaders.entrySet()){
            headers.set(entry.getKey().toString(),entry.getValue().toString());
        }

        HttpEntity<B> request = new HttpEntity<>(jsonBody, headers);
        return atlassianHostRestClients.authenticatedAsAddon().exchange(getFullPath() + relativeUrl, method, request, responseType).getBody();
    }

    protected String getFullPath() {
        return cloudSiteUrl + ATLASSIAN_CONNECT_MIGRATION_URL;
    }
}
