package com.vendor.cloudapp.local.workflow;

import com.atlassian.connect.spring.IgnoreJwt;
import com.vendor.cloudapp.local.BaseController;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@IgnoreJwt
@RestController
@RequestMapping("workflow")
public class LocalWorkflowController extends BaseController {

    /**
     * Use this API to retrive the migrated workflowRules ids, and then use:
     *  https://developer.atlassian.com/cloud/jira/platform/rest/v3/api-group-workflow-transition-rules/#api-rest-api-3-workflow-rule-config-put to update the rules
     *  https://developer.atlassian.com/cloud/jira/platform/rest/v3/api-group-workflow-transition-rules/#api-rest-api-3-workflow-rule-config-delete-put to delete the migrated rules
     * @param transferId
     * @param workflowsRulesRequest
     * @return
     */
    @RequestMapping(value = "/search", method = RequestMethod.POST)
    public ResponseEntity<String> searchWorkflows(@RequestHeader("Atlassian-Transfer-Id") String transferId, @RequestBody WorkflowsRulesRequest workflowsRulesRequest) {
        Map<String, String> headers = new HashMap<>();
        headers.put("Atlassian-Transfer-Id", transferId);
        return callWithAdditionalHeaders("/workflow/rule/search", HttpMethod.POST, workflowsRulesRequest, String.class, headers);
    }
}
