# Atlassian Connect Sample  
  
To run this project you will need some prerequisites:  
  
 - A Jira Cloud Site.
 - ngrok (https://ngrok.com/)

## Step-by-step:

### 1 - Run Ngrok

    ngrok http 9090

save your Ngrok endpoint, ex: https://570a32f41.ngrok.io.

### 2 - Update your settings

Go to `appllicaiton.yml` and set your environment. Specifically the variables given as example below:

```yaml
cloud.site.url: https://your-cloud-site.atlassian.net/
addon:
  key: my-cloud-custom-field-app-key
```

### 3 - Run the project

```shell
mvn spring-boot:run
```

The project will expose the Atlassian Connect file endpoint:
https://570a32f41.ngrok.io/atlassian-connect.json ( you will use this endpoint to install the app on the cloud ).

### 4 - Upload the App on Cloud Site

This step will let you upload your app to the cloud-site. This means your app's endpoints are registered with the
cloud-site. Therefore, cloud-site could send request to your app in case of product related events -  (e.g. Issue update
for JIRA).

#### On Jira

Go to:  `Settings -> Manage Apps -> click on Settings -> Enable development mode`
On the Manage Apps page: click on Upload App -> use your Atlassian Connect address ( https://570a32f41.ngrok.io/atlassian-connect.json  )

***Obs***: there is a file inside the project: *addon-db.script*. Clear every line that looks like " *INSERT INTO ATLASSIAN_HOST VALUES('6319dcf0-70f6-38c9-930d-42ea959c9f86 ...."*, otherwise the app will use this token to validate the install, but this could be an old token. If you stopped Ngrok for any reason then you should remove this line and run the install process again.

### 5 - App Migration Endpoints

In the sample app(src/main/java/com/vendor/cloudapp/local), there are following controllers available to show App Migration related Endpoints.

The `clientKey` query parameter needed can be found when you've installed the Connect app, you can check the logs for the following lines after installing:
```
c.v.cloudapp.connect.InstallController   : Enabled event: {"key":"com.vendor.plugin-workflow-sample","clientKey":"50553376-xxxx-xxxx-xxxx-xxxxxxxxxxxx", ....
```
More info on `clientKey` here: https://developer.atlassian.com/cloud/jira/platform/understanding-jwt-for-connect-apps/#decoding-and-verifying-a-jwt-token

 - WebhookRegistrationController: This shows how to GET/PUT webhooks related to App Migration events. For DELETE - use PUT /webhook with empty list of notification endpoints.
 - MappingMigrationController: This shows the following types of requests:
    - /migration/mapping/{transferId}/page?namespace={val}: Returns mappings for a given namespace and transferId, using pagination. Check MappingMigrationController handler for the usage of lastEntity, and pageSize query parameters.
    - /migration/field : Bulk API to update the custom field values in an issue
 One can send request to http://localhost:9090 to those controllers and those controllers will send the request to Cloud-site. This is just to show how the communication should be done from your cloud app to cloud-site.

### 6 - App Migration Related Events

After registering webhooks, your cloud app will be notified for the migration related events on the webhooks you registered on step 5. The structure of the event is shown by MigrationEvent class.

### 7 - [Optional] Customising Webhooks For Products - JIRA

You can choose which events will be sent to your app. 
 - Check the details [https://developer.atlassian.com/cloud/jira/platform/modules/webhook/](https://developer.atlassian.com/cloud/jira/platform/modules/webhook/)
 - Update the atlassian-connect.json file on the project (a re-install will be necessary)
 - Remove the lines from addon-db.script file

## References

Check some links from the official public doc about Atlassian Connect and Integrating with Jira Cloud.
[https://connect-inspector.prod.public.atl-paas.net/page/start](https://connect-inspector.prod.public.atl-paas.net/page/start)
[https://ngrok.com/docs#inspect](https://ngrok.com/docs#inspect)
[https://developer.atlassian.com/cloud/jira/platform/modules/webhook/](https://developer.atlassian.com/cloud/jira/platform/modules/webhook/)
[https://developer.atlassian.com/cloud/jira/platform/getting-started-with-connect/](https://developer.atlassian.com/cloud/jira/platform/getting-started-with-connect/)
[https://developer.atlassian.com/cloud/jira/platform/connect-patterns-and-examples/](https://developer.atlassian.com/cloud/jira/platform/connect-patterns-and-examples/)
[https://developer.atlassian.com/cloud/jira/platform/connect-tutorials-and-guides/](https://developer.atlassian.com/cloud/jira/platform/connect-tutorials-and-guides/)

## License

Copyright (c) 2021 Atlassian and others. Apache 2.0 licensed,
see [LICENSE.txt](https://bitbucket.org/atlassianlabs/app-migration-vendor-example/src/master/LICENSE.txt) file.

