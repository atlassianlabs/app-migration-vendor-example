package com.vendor.cloudapp;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.ResponseEntity;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;
import static org.springframework.http.HttpStatus.BAD_REQUEST;

@SpringBootTest(webEnvironment = RANDOM_PORT)
public class IntegrationTest {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Test
    public void shouldRejectRequestsWithoutJwt() {

        ResponseEntity<Void> stringResponseEntity = testRestTemplate.getForEntity("/rest/api/3/issue/1", Void.class);

        assertThat(stringResponseEntity.getStatusCode()).isEqualTo(BAD_REQUEST);
    }
}
