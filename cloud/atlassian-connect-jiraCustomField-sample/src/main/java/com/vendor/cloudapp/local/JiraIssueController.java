package com.vendor.cloudapp.local;

import com.atlassian.connect.spring.IgnoreJwt;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@IgnoreJwt
@RestController
@RequestMapping("/rest/api/3/issue/{issueId}")
public class JiraIssueController extends BaseController {

    /**
     * @param issueId The issueId or the key of the issue
     * @param clientKey This can be found after the app has been installed, check the logs for `Enabled event`
     */
    @PutMapping
    public ResponseEntity<Void> editIssueFields(@PathVariable String issueId, @RequestParam String clientKey) {
        return ResponseEntity.ok(call(getJiraFullPath() + "/rest/api/3/issue" + issueId,  HttpMethod.PUT, null, Void.class, retrieveAtlassianHost(clientKey)));
    }

    /**
     * @param issueId The issueId or the key of the issue
     * @param clientKey This can be found after the app has been installed, check the logs for `Enabled event`
     */
    @GetMapping
    public ResponseEntity<String> getIssueFields(@PathVariable String issueId, @RequestParam String clientKey) {
        return ResponseEntity.ok(call(getJiraFullPath() + "/rest/api/3/issue/" + issueId , HttpMethod.GET, null, String.class, retrieveAtlassianHost(clientKey)));
    }
}
