package com.vendor.cloudapp.local;

import com.atlassian.connect.spring.IgnoreJwt;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;
import java.util.Set;

@IgnoreJwt
@RestController
@RequestMapping("migration")
public class MappingMigrationController extends BaseController {

    /**
     * <a href="https://developer.atlassian.com/platform/app-migration/rest/api-group-mapping-api/#api-mapping-transferid-page-get">Rest API Ref</a>
     * @param transferId The transferId whose mappings you want to retrieve
     * @param clientKey  This can be found after the app has been installed, check the logs for `Enabled event`
     * @param namespace  The namespace to use. More info: <a href="https://developer.atlassian.com/platform/app-migration/mappings/#mappings-namespaces-and-entities">here</a>
     * @param lastEntity (optional) Pagination parameter, used to get the next page
     * @param pageSize   (optional) Pagination parameter, the size of the mapping page
     */
    @GetMapping(value = "/mapping/{transferId}/page", produces = "application/json")
    public ResponseEntity<String> getMappingsForTransferIdByPage(@PathVariable String transferId, @RequestParam String clientKey, @RequestParam String namespace, @RequestParam(required = false) String lastEntity, @RequestParam(required = false) String pageSize) {
        return ResponseEntity.ok(call(getFullPath() + "/mapping/" + transferId + "/page?" + paginationQueryParamsNameSpace(lastEntity, pageSize, namespace), HttpMethod.GET, null, String.class, retrieveAtlassianHost(clientKey)));
    }

    /**
     *<a href="https://developer.atlassian.com/platform/app-migration/rest/api-group-mapping-api/#api-mapping-transferid-find-post">Rest API Ref</a>
     * @param transferId The transferId whose mappings you want to retrieve
     * @param clientKey  This can be found after the app has been installed, check the logs for `Enabled event`
     * @param namespace  The namespace to use. More info: <a href="https://developer.atlassian.com/platform/app-migration/mappings/#mappings-namespaces-and-entities">here</a>
     * @param ids        Keys to retrieve
     */
    @PostMapping(value = "/mapping/{transferId}/find", produces = "application/json")
    public ResponseEntity<Map> getMappingsForTransferIdByKeys(@PathVariable String transferId, @RequestParam String clientKey, @RequestParam String namespace, @RequestBody Set<String> ids) {
        return ResponseEntity.ok(call(getFullPath() + "/mapping/" + transferId + "/find?namespace=" + namespace, HttpMethod.POST, ids, Map.class, retrieveAtlassianHost(clientKey)));
    }


    /**
     * <a href="https://developer.atlassian.com/cloud/jira/platform/rest/v3/api-group-app-migration/#api-rest-atlassian-connect-1-migration-field-put">Rest API Ref</a>
     *
     * @param transferId                    The transferId whose mappings you want to retrieve
     * @param clientKey                     This can be found after the app has been installed, check the logs for `Enabled event`
     * @param jiraIssueFieldValueRequestDto The list of custom field update details.
     */
    @PutMapping(value = "/field", produces = "application/json")
    public ResponseEntity<Void> editCustomFieldValueInIssues(@RequestHeader("Atlassian-Transfer-Id") String transferId, @RequestParam String clientKey, @RequestBody JiraIssueFieldValueRequestDto jiraIssueFieldValueRequestDto) {
        return ResponseEntity.ok(callWithCustomHeader(getFullPath() + "/field", HttpMethod.PUT, jiraIssueFieldValueRequestDto, Void.class, transferId, retrieveAtlassianHost(clientKey)));
    }

    private String paginationQueryParamsNameSpace(String lastEntity, String pageSize, String namespace) {
        String result = "";
        result += getPaginationQueryParam(lastEntity, pageSize);
        if (StringUtils.hasText(namespace)) result += "&namespace=" + namespace;
        return result.replaceFirst("&", "");
    }

    private String getPaginationQueryParam(String lastEntity, String pageSize) {
        String result = "";
        if (StringUtils.hasText(lastEntity)) result += "&lastEntity=" + lastEntity;
        if (StringUtils.hasText(pageSize)) result += "&pageSize=" + pageSize;
        return result;
    }
}
