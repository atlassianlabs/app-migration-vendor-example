package com.vendor.cloudapp.connect;

public class MigrationEvent {

    private String messageId;
    private String cloudAppKey;
    private String transferId;
    private MigrationDetails migrationDetails;
    private String eventType;
    private String s3Key;
    private String label;

    public String getS3Key() {
        return s3Key;
    }

    public void setS3Key(String s3Key) {
        this.s3Key = s3Key;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getMessageId() { return messageId; }

    public void setMessageId(String messageId) { this.messageId = messageId; }

    public String getCloudAppKey() {
        return cloudAppKey;
    }

    public void setCloudAppKey(String cloudAppKey) {
        this.cloudAppKey = cloudAppKey;
    }

    public String getTransferId() {
        return transferId;
    }

    public void setTransferId(String transferId) {
        this.transferId = transferId;
    }

    public MigrationDetails getMigrationDetails() {
        return migrationDetails;
    }

    public void setMigrationDetails(MigrationDetails migrationDetails) {
        this.migrationDetails = migrationDetails;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    @Override
    public String toString() {
        return "MigrationEvent{" +
                "messageId='" + messageId + '\'' +
                ", cloudAppKey='" + cloudAppKey + '\'' +
                ", transferId='" + transferId + '\'' +
                ", migrationDetails=" + migrationDetails +
                ", eventType='" + eventType + '\'' +
                ", s3Key='" + s3Key + '\'' +
                ", label='" + label + '\'' +
                '}';
    }
}
