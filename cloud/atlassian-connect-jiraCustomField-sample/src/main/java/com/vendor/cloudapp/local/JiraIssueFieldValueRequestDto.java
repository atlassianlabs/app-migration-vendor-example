package com.vendor.cloudapp.local;


import java.util.ArrayList;
import java.util.List;

public class JiraIssueFieldValueRequestDto
{
    private  List<JiraIssueFieldValue> updateValueList = new ArrayList<JiraIssueFieldValue>();

    public List<JiraIssueFieldValue> getUpdateValueList() {
        return updateValueList;
    }

    public void setUpdateValueList(List<JiraIssueFieldValue> list) {
        updateValueList.addAll(list);
    }


}
