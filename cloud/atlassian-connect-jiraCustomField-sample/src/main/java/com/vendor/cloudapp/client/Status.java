package com.vendor.cloudapp.client;

public enum Status {
    IN_PROGRESS,
    SUCCESS,
    FAILED,
    INCOMPLETE
}

