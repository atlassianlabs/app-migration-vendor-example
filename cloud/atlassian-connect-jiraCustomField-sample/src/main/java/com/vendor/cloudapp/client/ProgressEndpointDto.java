package com.vendor.cloudapp.client;

public record ProgressEndpointDto(Status status, int percent, String message) {
}

