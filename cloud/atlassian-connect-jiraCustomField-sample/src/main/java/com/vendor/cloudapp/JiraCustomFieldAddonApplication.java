package com.vendor.cloudapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JiraCustomFieldAddonApplication {

    public static void main(String[] args)  {
        new SpringApplication(JiraCustomFieldAddonApplication.class).run(args);
    }
}
