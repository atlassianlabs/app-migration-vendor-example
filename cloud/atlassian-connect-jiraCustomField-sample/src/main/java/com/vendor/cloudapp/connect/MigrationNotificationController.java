package com.vendor.cloudapp.connect;

import com.vendor.cloudapp.client.AtlassianConnectClient;
import com.vendor.cloudapp.client.ProgressEndpointDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.vendor.cloudapp.client.Status.*;
import static java.lang.Thread.sleep;
import static java.time.Duration.ofMinutes;

@RestController
@RequestMapping("notification")
public class MigrationNotificationController {

    @Autowired
    protected AtlassianConnectClient atlassianConnectClient;

    private static final Logger logger = LoggerFactory.getLogger(MigrationNotificationController.class);

    @PostMapping
    public ResponseEntity<String> event(@RequestBody MigrationEvent event) {
        logger.info("Received webhook event notification: {}", event);

        // When listener-triggered notification is received, get app specific custom fields mappings using namespace jira/classic:appCustomField.
        // Follow step 2 and 3 described in https://developer.atlassian.com/platform/app-migration/migrating-app-custom-fields to add custom field values in migrated project issues.
        if ("app-data-uploaded".equals(event.getEventType())) {
            logger.info("s3Key to get url for uploaded data : {}", event.getS3Key());
            logger.info("label of uploaded data: {}", event.getLabel());

            // We need to acknowledge the event by returning a HTTP 200 response in less than 5 seconds.
            // The actual processing of the event should be done asynchronously.
            new Thread(() -> doBackgroundProcessing(event)).start();
        }
        return ResponseEntity.ok().build();
    }

    private void doBackgroundProcessing(MigrationEvent event) {
        String transferId = event.getTransferId();
        try {
            atlassianConnectClient.updateProgress(
                    transferId,
                    new ProgressEndpointDto(IN_PROGRESS, 50, "Processing file..."));

            sleep(ofMinutes(2).toMillis()); // Simulates some processing time

            atlassianConnectClient.updateProgress(
                    transferId,
                    new ProgressEndpointDto(SUCCESS, 100, "Migration completed"));
        } catch (Exception e) {
            logger.error("Error while processing event", e);
            atlassianConnectClient.updateProgress(
                    transferId,
                    new ProgressEndpointDto(FAILED, 100, "Migration failure"));
        }
    }
}
