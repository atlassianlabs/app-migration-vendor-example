package com.vendor.cloudapp.local;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.IgnoreJwt;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * This is no longer the preferred way to register webhook
 *
 * @see <a href="https://developer.atlassian.com/platform/app-migration/events/#app-migration-webhook-in-connect-descriptor">App Migration Webhook In Connect Descriptor</a>
 * for a more reliable way to register webhooks
 */
@IgnoreJwt
@RestController
@RequestMapping("webhook")
public class WebhookRegistrationController extends BaseController {

    /**
     * @param webhook webhook endpoints to be registered
     * @param clientKey This can be found after the app has been installed, check the logs for `Enabled event`
     */
    @PutMapping
    public ResponseEntity<Void> putWebHook(@RequestBody NotificationEndpoints webhook, @RequestParam String clientKey) {
        AtlassianHost atlassianHost = retrieveAtlassianHost(clientKey);
        return ResponseEntity.ok(call(getFullPath() + "/webhook", HttpMethod.PUT, webhook, Void.class, atlassianHost));
    }

    /**
     * @param clientKey This can be found after the app has been installed, check the logs for `Enabled event`
     */
    @GetMapping
    public ResponseEntity<String> getWebHook(@RequestParam String clientKey) {
        AtlassianHost atlassianHost = retrieveAtlassianHost(clientKey);
        return ResponseEntity.ok(call(this.getFullPath() + "/webhook", HttpMethod.GET, null, String.class, atlassianHost));
    }
}
