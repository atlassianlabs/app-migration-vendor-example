package com.vendor.cloudapp.local;

public enum JiraIssueFieldType {
    StringIssueField,
    NumberIssueField,
    TextIssueField,
    SingleSelectIssueField,
    RichTextIssueField,
    MultiSelectIssueField
}
