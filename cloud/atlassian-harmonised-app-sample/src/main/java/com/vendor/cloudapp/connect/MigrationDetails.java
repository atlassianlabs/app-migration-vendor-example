package com.vendor.cloudapp.connect;

import java.util.Collection;

public class MigrationDetails {

    private String migrationId;
    private String migrationScopeId;
    private String name;
    private Long createdAt;
    private String jiraClientKey;
    private String confluenceClientKey;
    private String cloudUrl;

    public String getMigrationId() {
        return migrationId;
    }

    public void setMigrationId(String migrationId) {
        this.migrationId = migrationId;
    }

    public String getMigrationScopeId() {
        return migrationScopeId;
    }

    public void setMigrationScopeId(String migrationScopeId) {
        this.migrationScopeId = migrationScopeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Long createdAt) {
        this.createdAt = createdAt;
    }

    public String getJiraClientKey() {
        return jiraClientKey;
    }

    public void setJiraClientKey(String jiraClientKey) {
        this.jiraClientKey = jiraClientKey;
    }

    public String getConfluenceClientKey() {
        return confluenceClientKey;
    }

    public void setConfluenceClientKey(String confluenceClientKey) {
        this.confluenceClientKey = confluenceClientKey;
    }

    public String getCloudUrl() {
        return cloudUrl;
    }

    public void setCloudUrl(String cloudUrl) {
        this.cloudUrl = cloudUrl;
    }

    @Override
    public String toString() {
        return "Migration Details{" +
                "migrationId='" + migrationId + '\'' +
                ", migrationScopeId='" + migrationScopeId + '\'' +
                ", name='" + name + '\'' +
                ", createdAt=" + createdAt +
                ", jiraClientKey='" + jiraClientKey + '\'' +
                ", confluenceClientKey='" + confluenceClientKey + '\'' +
                ", cloudUrl='" + cloudUrl + '}';
    }
}
