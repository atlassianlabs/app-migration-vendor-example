# Atlassian Harmonised App Sample  

This is a [sample harmonised app](https://developer.atlassian.com/platform/adopting-forge-from-connect/how-to-adopt/) to
demonstrate registration of app migration webhook endpoint using app migration connect module in Forge manifest.
  
To run this project you will need some prerequisites:  
  
 - Confluence / Jira Cloud Site.   
 - ngrok (https://ngrok.com/)
 - [Forge development environment](https://developer.atlassian.com/platform/forge/getting-started/) 
 
## Step-by-step:

#### 1 - Run Ngrok

    ngrok http 9090

save your Ngrok endpoint, `e.g. https://570a32f41.ngrok.io`.

#### 2 - Update your settings

**To install your app in Confluence cloud, update your settings as explained below**

   1. Go to `src/main/resources/application.yml` and update following configurations to match your environment :
      ```yaml
      cloud.site.url: https://your-cloud-site.atlassian.net/
      atlassian.product: confluence
      addon:
        key: my-cloud-app-key
        base-url: https://570a32f41.ngrok.io
      ```
   2. Go to `manifest.yml`,  update following configurations to match your environment :
      
      _NOTE : No need to update app.id, as it will be generated in later steps when running `forge register` command_
      ```yaml
      app:
        id: ari:cloud:ecosystem::app/00000000-0000-0000-0000-000000000000
        connect:
          key: my-cloud-app-key
          remote: connect
          authentication: jwt
        runtime:
          name: nodejs20.x
      remotes:
        - key: connect
          baseUrl: https://570a32f41.ngrok.io
      ```

**To install your app in Jira cloud, update your settings as explained below**

1. Go to `src/main/resources/application.yml` and update following configurations to match your environment :
      ```yaml
      cloud.site.url: https://your-cloud-site.atlassian.net/
      atlassian.product: jira
      addon:
        key: my-cloud-app-key
        base-url: https://570a32f41.ngrok.io
      ```
2. Rename existing `manifest.yml` to `manifest-confluence.yml` and rename `manifest-jira.yml` to `manifest.yml`
3. In your `manifest.yml`,  update following configurations to match your environment :

   _NOTE : No need to update app.id, as it will be generated in later steps when running `forge register` command_
   ```yaml
   app:
     id: ari:cloud:ecosystem::app/00000000-0000-0000-0000-000000000000
     connect:
       key: my-cloud-app-key
       remote: connect
       authentication: jwt
     runtime:
       name: nodejs20.x
   remotes:
     - key: connect
       baseUrl: https://570a32f41.ngrok.io
   ```

#### 3 - Run the project

```shell
mvn spring-boot:run
```

#### 4 - Register, deploy and install your app

Once you have logged into the CLI (`forge login`), follow the steps below to install the app onto your site:

- Run `forge register` to register a new copy of this app to your developer account
- Run `forge deploy` to deploy the app into default environment
- Run `forge install` and follow the prompts to install the app into default environment

#### 5 - App Migration Endpoints

In the sample app(src/main/java/com/vendor/cloudapp/local), `MappingMigrationController` demonstrates the invocation of App Migration Connect Endpoints.  
    
One can send request to http://localhost:9090 to these endpoints, which will send the request to App Migration Connect Endpoints. This is to show how the communication should be done from your cloud app to App Migration Connect Endpoints.

#### 6 - App Migration Related Events

Your cloud app will be notified for app migration related events on webhook endpoint you registered using `confluence:cloudAppMigration` or `jira:cloudAppMigration` in your Forge manifest. The structure of the event is shown by MigrationEvent class.

# References
- https://developer.atlassian.com/platform/forge/getting-started/
- https://developer.atlassian.com/platform/adopting-forge-from-connect/
- https://developer.atlassian.com/platform/app-migration/

# License

Copyright (c) 2020 Atlassian and others.
Apache 2.0 licensed, see [LICENSE.txt](https://bitbucket.org/atlassianlabs/app-migration-vendor-example/src/master/LICENSE.txt) file.