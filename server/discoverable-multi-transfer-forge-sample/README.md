# Discoverable Multi-Transfer Forge Listener Example App

This example uses the `atlassian-app-cloud-migration-listener` library to provide a simple working example using
Atlassian Spring Scanner with no OSGi dependencies. 

Included in this app is:
- A demo on how to use the `MultiTransferDiscoverableForgeListener` interface
- An example pre-migration check which implements `PreMigrationCheckRepository`


## How to use this library

- Import the `atlassian-app-cloud-migration-listener` dependency:

```xml
<dependency>
    <groupId>com.atlassian</groupId>
    <artifactId>atlassian-app-cloud-migration-listener</artifactId>
    <version>1.5.8</version>
</dependency>
``` 

- In `atlassian-plugin.xml` export a component that implements `MultiTransferDiscoverableForgeListener`

```xml
<component key="myPlugin" class="com.vendor.impl.MyPluginComponentImpl" public="true">
    <interface>com.atlassian.migration.app.listener.MultiTransferDiscoverableForgeListener</interface>
</component>
```

- In `atlassian-plugin.xml` export a component that implements `PreMigrationCheckRepository`

```xml
<component key="myCheckRepo" class="com.vendor.impl.MyCheckRepositoryImpl" public="true">
    <interface>com.atlassian.migration.app.check.PreMigrationCheckRepository</interface>
</component>
```

This will produce a P2-plugin jar which can be installed into a running Confluence or Jira server using (adjust as
necessary)

# License

Copyright (c) 2024 Atlassian and others. Apache 2.0 licensed,
see [LICENSE.txt](https://bitbucket.org/atlassianlabs/app-migration-vendor-example/src/master/LICENSE.txt) file.
