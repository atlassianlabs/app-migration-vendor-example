package com.vendor.impl;

import com.atlassian.migration.app.*;
import com.atlassian.migration.app.disco.MultiTransferDiscoverableForgeListener;
import com.atlassian.migration.app.disco.TransferDefinition;
import com.atlassian.migration.app.disco.TransferInfo;
import com.atlassian.migration.app.gateway.AppCloudForgeMigrationGateway;
import com.atlassian.migration.app.gateway.MigrationDetailsV1;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Named;
import java.io.IOException;
import java.io.OutputStream;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.atlassian.migration.app.AccessScope.*;
import static java.util.UUID.fromString;

@Named
@ExportAsService
public class ForgeMigrationListener implements MultiTransferDiscoverableForgeListener {

    private static final Logger log = LoggerFactory.getLogger(ForgeMigrationListener.class);

    /**
     * An example transfer definition list for a migration
     * All steps required to complete a migration must be defined here.
     */
    public Set<TransferDefinition> getTransferDefinitions() {
        // The transfer definitions required to complete a migration.
        // The definitions take a unique name for the transfer and whether that particular transfer is blocking or non-blocking.
        TransferDefinition firstTransfer = new TransferDefinition("blocking-transfer-name", true);
        TransferDefinition secondTransfer = new TransferDefinition("non-blocking-transfer-name", false);

        HashSet<TransferDefinition> transferDefinitions = new HashSet<>();
        transferDefinitions.add(firstTransfer);
        transferDefinitions.add(secondTransfer);

        return transferDefinitions;
    }

    /**
     * Just some examples of the operations that can be done as part of an app migration.
     * All of them are optional. You may or may not use them depending on your migration needs
     */
    public void onStartAppMigration(AppCloudForgeMigrationGateway gateway, TransferInfo transferInfo, MigrationDetailsV1 migrationDetails) {
        // Identify the transfer that is being processed
        if ("blocking-transfer-name".equals(transferInfo.getTransferName())) {
            // Retrieving ID mappings from the migration.
            // A complete guide can be found at https://developer.atlassian.com/platform/app-migration/getting-started/mappings/
            log.info("Migration context summary: " + migrationDetails);
            PaginatedMapping paginatedMapping = gateway.getPaginatedMapping("identity:user", 5);
            while (paginatedMapping.next()) {
                Map<String, String> mappings = paginatedMapping.getMapping();
                log.info("mappings = {}", mappings);
            }

            // If you know exactly the entities you are looking for a migration mapping, you can query them directly by IDs (up to 100 in a single request)
            gateway.getMappingById("identity:user", Collections.singleton("email/admin@example.com"));

            // Fetching containers associated with a migration
            // Following snippet shows fetching site containers associated with a migration
            // You can also fetch confluence space containers (ContainerType.ConfluenceSpace), jira project containers (ContainerType.JiraProject)
            // by providing corresponding container types in the call to "getPaginatedContainers"
            PaginatedContainers paginatedSiteContainers = gateway.getPaginatedContainers(ContainerType.Site, 1);
            while (paginatedSiteContainers.next()) {
                List<ContainerV1> siteContainers = paginatedSiteContainers.getContainers();
                for (ContainerV1 container : siteContainers) {
                    log.info("Site selections: " + ((SiteContainerV1) container).getSelections());
                }
            }

            try {
                // You can also upload one or more files to the cloud. You'll be able to retrieve them through Atlassian Connect
                OutputStream firstDataStream = gateway.createAppData();
                // You can even upload big files in here
                firstDataStream.write("Your binary data goes here".getBytes());

                firstDataStream.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

            log.info("Completed files");
        } else if ("non-blocking-transfer-name".equals(transferInfo.getTransferName())) {
            try {
                // You can also upload one or more files to the cloud with labels. You'll be able to retrieve them through Atlassian Connect
                OutputStream datastream = gateway.createAppData("my-label");
                // You can even upload big files in here
                datastream.write("Your binary data goes here".getBytes());

                datastream.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }

            log.info("Completed label files");
        }

        // Once you have exported all the data, you can call completeExport to enable progress reporting
        gateway.completeExport();
    }

    /**
     * this is app id of your forge app, they need to match since customfields will be something like:
     * <forge app id>:<forge env uuid>:<custom field id>
     */
    @Override
    public UUID getForgeAppId() {
        return fromString("your-forge-app-id-here");
    }

    /**
     * this is the forge environment of your app, they need to match since custom fields will be something like:
     * <forge app id>:<forge env uuid>:<custom field id>
     */
    @Override
    public String getForgeEnvironmentName() {
        return ForgeEnvironmentName.DEVELOPMENT;
    }


    /**
     * Declare what categories of data your app handles.
     */
    @Override
    public Set<AccessScope> getDataAccessScopes() {
        return Stream.of(APP_DATA_OTHER, PRODUCT_DATA_OTHER, MIGRATION_TRACING_IDENTITY, MIGRATION_TRACING_PRODUCT)
                .collect(Collectors.toCollection(HashSet::new));
    }

}
