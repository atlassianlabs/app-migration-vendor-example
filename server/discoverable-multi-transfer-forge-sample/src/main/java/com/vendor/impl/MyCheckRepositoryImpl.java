package com.vendor.impl;

import com.atlassian.migration.app.check.*;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Named;
import java.util.*;

import static com.atlassian.migration.app.check.CheckStatus.*;
import static java.util.Collections.singletonMap;

/**
 * Optionally, you can provide your own collection of pre-migration checks to be executed prior a migration.
 *
 * @since JCMA 1.7.7 under the dark feature com.atlassian.jira.migration-assistant.enable.app-vendor-check
 * @since CCMA 3.4.3 and higher by default. This means you will no longer need to use `migration-assistant.enable.app-vendor-check` feature flag to enable this feature.
 * @since atlassian-app-cloud-migration-listener:1.1.0
 */
@Named
@ExportAsService
public class MyCheckRepositoryImpl implements PreMigrationCheckRepository {

    private static final Logger log = LoggerFactory.getLogger(MyCheckRepositoryImpl.class);

    /**
     * This method returns a set of checks that will be executed before the migration starts.
     */
    @Override
    public Set<CheckSpec> getAvailableChecks() {
        HashSet<CheckSpec> checks = new HashSet<>();

        try {
            // You can have multiple checks, up to 3 checks will be executed. If there's more than that, some checks will be ignored.
            checks.add(createFirstCheck());
        } catch (IllegalArgumentException e) {
            // Invalid checkIds, title and stepsToResolve can throw this exception from CheckSpec's constructor
            e.printStackTrace();
        }

        return checks;
    }

    /**
     * Perform the checks for that specific checkId.
     * If you have multiple checks they may be executed in parallel.
     * This method must return in under 5 minutes to avoid time-outs.
     */
    @Override
    public CheckResult executeCheck(String checkId, MigrationPlanContext migrationPlanContext) {
        log.info("Running checks for plan {}", migrationPlanContext.getPlanName());
        log.info("Context of the migration: {}", Arrays.toString(migrationPlanContext.getContainers().toArray()));

        switch (checkId) {
            case "success-check":
                return new CheckResult(SUCCESS); // You probably want to run some checks. Here we just keep it simple
            default:
                throw new IllegalStateException("Unknown check: " + checkId);
        }
    }

    /**
     * An example of creating a check.
     * For more complicated checks please refer to the discoverable-sample project
     * @return
     */
    private static CheckSpec createFirstCheck() {
        // Creating our first check. CheckSpec will validate some base pre-conditions, like valid checkId.
        CheckSpec simplePermissionCheck = new CheckSpec(
                "success-check",                                 // Unique ID across all the implemented checks
                "Success check",                                                  // Check's title
                "Success check",                                            // A brief description
                singletonMap("template1", "This one always success") // A template that contains instructions on how to resolve this check
        );

        /** Before returning these checks we advise to invoke the `validate()` method. We truncate the content
         * based on the following rules:
         * `title` should not exceed 60 characters
         * `stepsToResolve` should not exceed 1050 characters
         *
         * validate() will return List<String> that contains these error messages
         */
        List<String> validationErrors = simplePermissionCheck.validate();
        if (!validationErrors.isEmpty()) {
            log.warn("Validation errors found: " + validationErrors);
        }

        return simplePermissionCheck;
    }
}