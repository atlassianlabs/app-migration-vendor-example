package com.vendor.impl;

import com.atlassian.migration.app.*;
import com.atlassian.migration.app.disco.MultiTransferDiscoverableListener;
import com.atlassian.migration.app.disco.TransferDefinition;
import com.atlassian.migration.app.disco.TransferInfo;
import com.atlassian.migration.app.gateway.AppCloudMigrationGateway;
import com.atlassian.migration.app.gateway.MigrationDetailsV1;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.OutputStream;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.atlassian.migration.app.AccessScope.*;

public class MyPluginComponentImpl implements MultiTransferDiscoverableListener {

    private static final Logger log = LoggerFactory.getLogger(MyPluginComponentImpl.class);

    /**
     * An example transfer definition list for a migration
     * All steps required to complete a migration must be defined here.
     */
    public Set<TransferDefinition> getTransferDefinitions() {
        // The transfer definitions required to complete a migration.
        // The definitions take a unique name for the transfer and whether that particular transfer is blocking or non-blocking.
        TransferDefinition firstTransfer = new TransferDefinition("blocking-transfer-name", true);
        TransferDefinition secondTransfer = new TransferDefinition("non-blocking-transfer-name", false);

        HashSet<TransferDefinition> transferDefinitions = new HashSet<>();
        transferDefinitions.add(firstTransfer);
        transferDefinitions.add(secondTransfer);

        return transferDefinitions;
    };

    /**
     * Just some examples of the operations that can be done as part of an app migration.
     * All of them are optional. You may or may not use them depending on your migration needs
     */
    public void onStartAppMigration(AppCloudMigrationGateway gateway, TransferInfo transferInfo, MigrationDetailsV1 migrationDetails) {
        try {
            // Identify the transfer that is being processed
            if ("blocking-transfer-name".equals(transferInfo.getTransferName())) {
                // Retrieving ID mappings from the migration.
                // A complete guide can be found at https://developer.atlassian.com/platform/app-migration/getting-started/mappings/
                log.info("Migration context summary: " + migrationDetails);
                PaginatedMapping paginatedMapping = gateway.getPaginatedMapping(transferInfo.getTransferId().toString(), "identity:user", 5);
                while (paginatedMapping.next()) {
                    Map<String, String> mappings = paginatedMapping.getMapping();
                    log.info("mappings = {}", mappings);
                }

                // If you know exactly the entities you are looking for a migration mapping, you can query them directly by IDs (up to 100 in a single request)
                gateway.getMappingById(transferInfo.getTransferId().toString(), "identity:user", Collections.singleton("email/admin@example.com"));

                // Fetching containers associated with a migration
                // Following snippet shows fetching site containers associated with a migration
                // You can also fetch confluence space containers (ContainerType.ConfluenceSpace), jira project containers (ContainerType.JiraProject)
                // by providing corresponding container types in the call to "getPaginatedContainers"
                PaginatedContainers paginatedSiteContainers = gateway.getPaginatedContainers(transferInfo.getTransferId().toString(), ContainerType.Site, 1);
                while (paginatedSiteContainers.next()) {
                    List<ContainerV1> siteContainers = paginatedSiteContainers.getContainers();
                    for (ContainerV1 container : siteContainers) {
                        log.info("Site selections: " + ((SiteContainerV1) container).getSelections());
                    }
                }

                // Your app counterparty in cloud can send arbitrary data to guide the export process. They can be accessed via polling
                gateway.getCloudFeedbackIfPresent(transferInfo.getTransferId().toString()).ifPresent(it -> log.info("Cloud feedback found"));

                // You can also upload one or more files to the cloud. You'll be able to retrieve them through Atlassian Connect
                OutputStream firstDataStream = gateway.createAppData(transferInfo.getTransferId().toString());
                // You can even upload big files in here
                firstDataStream.write("Your binary data goes here".getBytes());
                firstDataStream.close();

            } else if ("non-blocking-transfer-name".equals(transferInfo.getTransferName())) {
                // You can upload metadata to support your import process
                OutputStream dataStream = gateway.createAppData(transferInfo.getTransferId().toString(), "some-optional-label");
                dataStream.write("more bytes".getBytes());
                dataStream.close();
            }
        } catch (IOException e) {
            log.error("Error while running the migration", e);
        }
    }

    @Override
    public String getCloudAppKey() {
        return "my-cloud-app-key";
    }

    /**
     * Declare what categories of data your app handles.
     */
    @Override
    public Set<AccessScope> getDataAccessScopes() {
        return Stream.of(APP_DATA_OTHER, PRODUCT_DATA_OTHER, MIGRATION_TRACING_IDENTITY, MIGRATION_TRACING_PRODUCT)
                .collect(Collectors.toCollection(HashSet::new));
    }
}
