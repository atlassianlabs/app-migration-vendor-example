# Discoverable Multi-Transfer App -  A Migration Listener

This example uses the `atlassian-app-cloud-migration-listener` library to provide a simple working example using Atlassian Spring Scanner with no OSGi dependencies

## How to use this library

- Import the dependency:
```xml
<dependency>
    <groupId>com.atlassian</groupId>
    <artifactId>atlassian-app-cloud-migration-listener</artifactId>
    <version>1.5.8</version>
</dependency>
``` 

- In `atlassian-plugin.xml` export a component that implements `MultiTransferDiscoverableListener`

```xml
<component key="myPlugin" class="com.vendor.impl.MyPluginComponentImpl" public="true">
    <interface>com.atlassian.migration.app.listener.MultiTransferDiscoverableListener</interface>
</component>
```

Starting from JCMA v1.7.7, CCMA v3.4.0, you can also optionally export your own pre-migration checks.

```xml
<component key="myCheckRepo" class="com.vendor.impl.MyCheckRepositoryImpl" public="true">
    <interface>com.atlassian.migration.app.check.PreMigrationCheckRepository</interface>
</component>
```

## Building

Go to server app project root folder and then go to `server/discoverable-multi-transfer-sample` folder, build the project with:

```shell
mvn clean package
```

This will produce a P2-plugin jar which can be installed into a running Confluence or Jira server using (adjust as
necessary)
```shell
atlas-install-plugin --http-port 1990 --context-path /confluence
```

# License

Copyright (c) 2023 Atlassian and others.
Apache 2.0 licensed, see [LICENSE.txt](https://bitbucket.org/atlassianlabs/app-migration-vendor-example/src/master/LICENSE.txt) file.
