package com.vendor.impl;

import com.atlassian.migration.app.AccessScope;
import com.atlassian.migration.app.gateway.AppCloudMigrationGateway;
import com.atlassian.migration.app.gateway.MigrationDetailsV1;
import com.vendor.MyPluginComponent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.atlassian.migration.app.AccessScope.*;

public class MyPluginComponentImpl implements MyPluginComponent {

    private static final Logger log = LoggerFactory.getLogger(MyPluginComponentImpl.class);

    /**
     * Just some examples of the operations that can be done as part of an app migration.
     * All of them are optional. You may or may not use them depending on your migration needs
     */
    @Override
    public void onStartAppMigration(AppCloudMigrationGateway migrationGateway, String transferId, MigrationDetailsV1 migrationDetails) {
    }

    @Override
    public Map<String, String> getSupportedWorkflowRuleMappings() {
        Map<String, String> workflowRules = new HashMap<>();
        workflowRules.put("com.vendor.workflow.ParentIssueBlockingCondition", "parentIssueBlockingCondition-cloud");
        workflowRules.put("com.vendor.workflow.CloseParentIssuePostFunction", "closeParentIssuePostFunction-cloud");
        workflowRules.put("com.vendor.workflow.CloseIssueWorkflowValidator", "closeIssueWorkflowValidator-cloud");
        return workflowRules;
    }

    @Override
    public String getCloudAppKey() {
        return "com.vendor.plugin-workflow-sample";
    }


    /**
     * Declare what categories of data your app handles.
     */
    @Override
    public Set<AccessScope> getDataAccessScopes() {
        return Stream.of(APP_DATA_OTHER, PRODUCT_DATA_OTHER, MIGRATION_TRACING_PRODUCT)
                .collect(Collectors.toCollection(HashSet::new));
    }
}
