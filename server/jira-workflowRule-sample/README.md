# Workflow - sample -  A Migration Listener
This is the simplest we can get from a working example for app-migration.

It has no spring-scanner, no buffering against OSGi changes, and minimal dependencies.

## Important pieces to reproduce this

- Import this dependency:
```xml
<dependency>
    <groupId>com.atlassian</groupId>
    <artifactId>atlassian-app-cloud-migration-listener</artifactId>
    <version>1.1.0</version>
</dependency>
``` 
- Export an OSGi component that implements the interface `com.atlassian.migration.app.listener.DiscoverableListener` and `com.atlassian.migration.app.jira.JiraAppCloudMigrationListenerV1`

## Building

Go to server app project root folder and then go to server/workflow-sample folder, build the project with:

    mvn clean package

This will produce a P2-plugin jar.

# License

Copyright (c) 2023 Atlassian and others.
Apache 2.0 licensed, see [LICENSE.txt](https://bitbucket.org/atlassianlabs/app-migration-vendor-example/src/master/LICENSE.txt) file.
