package com.vendor;

import com.atlassian.migration.app.jira.JiraAppCloudMigrationListenerV1;
import com.atlassian.migration.app.listener.DiscoverableListener;

public interface MyPluginComponent extends JiraAppCloudMigrationListenerV1, DiscoverableListener {
}
