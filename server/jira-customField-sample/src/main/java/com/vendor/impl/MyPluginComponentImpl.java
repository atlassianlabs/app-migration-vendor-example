package com.vendor.impl;

import com.atlassian.migration.app.AccessScope;
import com.atlassian.migration.app.ServerAppCustomField;
import com.atlassian.migration.app.gateway.AppCloudMigrationGateway;
import com.atlassian.migration.app.gateway.MigrationDetailsV1;
import com.vendor.MyPluginComponent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.atlassian.migration.app.AccessScope.*;

public class MyPluginComponentImpl implements MyPluginComponent {

    private static final Logger log = LoggerFactory.getLogger(MyPluginComponentImpl.class);

    /**
     * Just some examples of the operations that can be done as part of an app migration.
     * All of them are optional. You may or may not use them depending on your migration needs
     */
    @Override
    public void onStartAppMigration(AppCloudMigrationGateway migrationGateway, String transferId, MigrationDetailsV1 migrationDetails) {
        // Retrieve ID mappings from the migration.
        // A complete guide can be found at https://developer.atlassian.com/platform/app-migration/getting-started/mappings/

        // Fetch containers associated with a migration
        // You can also fetch jira project containers using ContainerType.JiraProject as container type in the call to "getPaginatedContainers"

        // You can also upload one or more files to the cloud. You'll be able to retrieve them through Atlassian Connect
        // Refer file data updload example mentioned in https://bitbucket.org/atlassianlabs/app-migration-vendor-example/src/master/server/basic-sample/
    }

    @Override
    public Map<ServerAppCustomField, String> getSupportedCustomFieldMappings() {
        Map<ServerAppCustomField, String> mappings = new HashMap<>();
        mappings.put(new ServerAppCustomField("Sample Custom Field", "sample-custom-field-type"), "sample-custom-field-cloud");
        return mappings;
    }

    @Override
    public String getCloudAppKey() {
        return "my-cloud-custom-field-app-key";
    }

    /**
     * Declare what categories of data your app handles.
     */
    @Override
    public Set<AccessScope> getDataAccessScopes() {
        return Stream.of(APP_DATA_OTHER, PRODUCT_DATA_OTHER, MIGRATION_TRACING_PRODUCT)
                .collect(Collectors.toCollection(HashSet::new));
    }
}
