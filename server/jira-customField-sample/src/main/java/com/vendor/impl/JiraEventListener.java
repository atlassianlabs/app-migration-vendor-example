package com.vendor.impl;

import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.config.managedconfiguration.ConfigurationItemAccessLevel;
import com.atlassian.jira.config.managedconfiguration.ManagedConfigurationItem;
import com.atlassian.jira.config.managedconfiguration.ManagedConfigurationItemBuilder;
import com.atlassian.jira.config.managedconfiguration.ManagedConfigurationItemService;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.context.GlobalIssueContext;
import com.atlassian.jira.issue.customfields.CustomFieldType;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.plugin.event.events.PluginEnabledEvent;
import com.atlassian.plugin.spring.scanner.annotation.imports.JiraImport;
import org.ofbiz.core.entity.GenericEntityException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static com.vendor.impl.SampleCustomFieldType.CUSTOM_FIELD_TYPE_KEY;
import static java.util.Collections.singletonList;

@Component
public class JiraEventListener implements DisposableBean {
    private static final Logger log = LoggerFactory.getLogger(JiraEventListener.class);

    @JiraImport
    private final CustomFieldManager customFieldManager;

    @JiraImport
    private final EventPublisher eventPublisher;

    @Autowired
    public JiraEventListener(final EventPublisher eventPublisher, final CustomFieldManager customFieldManager) {
        this.eventPublisher = eventPublisher;
        this.customFieldManager = customFieldManager;
        eventPublisher.register(this);
    }

    /**
     * Called when the plugin is being disabled or removed.
     *
     * @throws Exception
     */
    @Override
    public void destroy() throws Exception {
        log.info("Disabling plugin");
        eventPublisher.unregister(this);
    }

    @EventListener
    public void onPluginEnabledEvent(PluginEnabledEvent evt) {
        if (!evt.getPlugin().getKey().equals("my-server-custom-field-app-key")) return;

        log.info("******** Sample Custom Field App enabled. Setting up Custom Fields...");

        // Obtain CF type
        CustomFieldType customFieldType = customFieldManager.getCustomFieldType(CUSTOM_FIELD_TYPE_KEY);
        CustomFieldType standardCustomFieldType = customFieldManager.getCustomFieldType("com.atlassian.jira.plugin.system.customfieldtypes:textfield");
        if (customFieldType == null) {
            log.warn("Custom Field type {} not found. WILL NOT create Custom Field.", CUSTOM_FIELD_TYPE_KEY);
            return;
        }

        final String fieldName = "Sample Custom Field";
        try {
            for (CustomField field : customFieldManager.getCustomFieldObjects()) {
                if (field.getName().startsWith(fieldName)) {
                    customFieldManager.removeCustomField(field);
                }
            }
        } catch (Exception e) {

        }
        log.info("Creating Custom Field '{}' with type {}...", fieldName, customFieldType.getKey());
        try {
            CustomField sampleCustomField = customFieldManager.createCustomField(fieldName, "Sample Custom Field App",
                    customFieldType, null,
                    singletonList(GlobalIssueContext.getInstance()), singletonList(null));
            lockTheCustomField(sampleCustomField);
            log.info("Successfully Created CF '{}' with ID {} ", fieldName, sampleCustomField.getId());
        } catch (GenericEntityException e) {
            log.error("Failed to create CF '{}'", fieldName, e);
        }
    }

    private void lockTheCustomField(CustomField customField) {
        ManagedConfigurationItemService managedConfigurationItemService = ComponentAccessor.getComponentOfType(ManagedConfigurationItemService.class);
        ManagedConfigurationItem field = managedConfigurationItemService.getManagedCustomField(customField);
        ManagedConfigurationItem managedField = ManagedConfigurationItemBuilder
                .builder(field)
                .setManaged(true)
                .setConfigurationItemAccessLevel(ConfigurationItemAccessLevel.LOCKED)
                .build();

        managedConfigurationItemService.updateManagedConfigurationItem(managedField);
    }

}
