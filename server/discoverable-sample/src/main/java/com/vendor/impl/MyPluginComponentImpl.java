package com.vendor.impl;

import com.atlassian.migration.app.*;
import com.atlassian.migration.app.gateway.AppCloudMigrationGateway;
import com.atlassian.migration.app.gateway.MigrationDetailsV1;
import com.atlassian.migration.app.listener.RerunnableDiscoverableListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.OutputStream;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.atlassian.migration.app.AccessScope.*;

public class MyPluginComponentImpl implements RerunnableDiscoverableListener {

    private static final Logger log = LoggerFactory.getLogger(MyPluginComponentImpl.class);

    /**
     * Just some examples of the operations that can be done as part of an app migration.
     * All of them are optional. You may or may not use them depending on your migration needs
     */
    public void onStartAppMigration(AppCloudMigrationGateway gateway, String transferId, MigrationDetailsV1 migrationDetails) {
        try {
            // Retrieving ID mappings from the migration.
            // A complete guide can be found at https://developer.atlassian.com/platform/app-migration/getting-started/mappings/
            log.info("Migration context summary: " + migrationDetails);
            PaginatedMapping paginatedMapping = gateway.getPaginatedMapping(transferId, "identity:user", 5);
            while (paginatedMapping.next()) {
                Map<String, String> mappings = paginatedMapping.getMapping();
                log.info("mappings = {}", mappings);
            }

            // If you know exactly the entities you are looking for a migration mapping, you can query them directly by IDs (up to 100 in a single request)
            gateway.getMappingById(transferId, "identity:user", Collections.singleton("email/admin@example.com"));

            // Fetching containers associated with a migration
            // Following snippet shows fetching site containers associated with a migration
            // You can also fetch confluence space containers (ContainerType.ConfluenceSpace), jira project containers (ContainerType.JiraProject)
            // by providing corresponding container types in the call to "getPaginatedContainers"
            PaginatedContainers paginatedSiteContainers = gateway.getPaginatedContainers(transferId, ContainerType.Site, 1);
            while (paginatedSiteContainers.next()) {
                List<ContainerV1> siteContainers = paginatedSiteContainers.getContainers();
                for (ContainerV1 container : siteContainers) {
                    log.info("Site selections: " + ((SiteContainerV1) container).getSelections());
                }
            }

            // Your app counterparty in cloud can send arbitrary data to guide the export process. They can be accessed via polling
            gateway.getCloudFeedbackIfPresent(transferId).ifPresent(it -> log.info("Cloud feedback found"));

            // You can also upload one or more files to the cloud. You'll be able to retrieve them through Atlassian Connect
            OutputStream firstDataStream = gateway.createAppData(transferId);
            // You can even upload big files in here
            firstDataStream.write("Your binary data goes here".getBytes());
            firstDataStream.close();

            // You can also apply labels to distinguish files or to add meta data to support your import process
            OutputStream secondDataStream = gateway.createAppData(transferId, "some-optional-label");
            secondDataStream.write("more bytes".getBytes());
            secondDataStream.close();

        } catch (IOException e) {
            log.error("Error while running the migration", e);
        }
    }

    /**
     * This method is called when the migration is re-run. You can use this method to clean up any data that was created
     * during the first migration run or to perform incremental migrations.
     *
     * @param migrationGateway      the gateway to access the migration data
     * @param originalTransferId    the transfer ID of the original migration
     * @param rerunTransferId       the transfer ID of the rerun migration
     * @param rerunMigrationDetails the migration details of the rerun migration
     * @since CCMA 3.4.7
     * @since JCMA 1.9.4
     */
    @Override
    public void onRerunAppMigration(AppCloudMigrationGateway migrationGateway, String originalTransferId, String rerunTransferId, MigrationDetailsV1 rerunMigrationDetails) {
        // Specific operations to be done when the migration is rerun by the user
        log.info("Triggering rerun migration for transferId: {}. Original transferId was {}", rerunTransferId, originalTransferId);
        // In this example, we are just delegating the call to onStartAppMigration
        this.onStartAppMigration(migrationGateway, rerunTransferId, rerunMigrationDetails);
    }

    @Override
    public String getCloudAppKey() {
        return "my-cloud-app-key";
    }

    /**
     * Declare what categories of data your app handles.
     */
    @Override
    public Set<AccessScope> getDataAccessScopes() {
        return Stream.of(APP_DATA_OTHER, PRODUCT_DATA_OTHER, MIGRATION_TRACING_IDENTITY, MIGRATION_TRACING_PRODUCT)
                .collect(Collectors.toCollection(HashSet::new));
    }
}
