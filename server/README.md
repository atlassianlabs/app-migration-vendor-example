App Migration - Server App Examples
==============

A collection of server apps (P2 plugins) that shows how to interact with the App Migarion platform. 

- discoverable-sample: Example of how to use `atlassian-app-cloud-migration-listener` dependency and its basic functionality. Components are exposed with Spring Scanner (XML).
- discoverable-sample-spring-scanner2: Example of how to use `atlassian-app-cloud-migration-listener` dependency and its basic functionality. Components are exposed with Spring Scanner 2 (annotations).
- jira-customField-sample: Example of how to migrate Jira custom fields.
- jira-workflowRule-sample: Example of how to migrate Jira workflow rules.

Usage
======

Inside each project folder you can:

* atlas-run   -- installs this plugin into the product and starts it on localhost
* atlas-debug -- same as atlas-run, but allows a debugger to attach at port 5005
* atlas-help  -- prints description for all commands in the SDK

Full documentation is always available at:

https://developer.atlassian.com/server/framework/atlassian-sdk/
