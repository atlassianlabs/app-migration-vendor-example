# Discoverable App - A Migration Listener

This example uses the `atlassian-app-cloud-migration-listener` library to provide a simple working example using
Atlassian Spring Scanner with no OSGi dependencies.

The example is specifically targeted on Apps that
use [Spring Scanner 2](https://bitbucket.org/atlassian/atlassian-spring-scanner/src/master/)

## How to use this library

- Import the dependency:

```xml

<dependency>
    <groupId>com.atlassian</groupId>
    <artifactId>atlassian-app-cloud-migration-listener</artifactId>
    <version>1.1.0</version>
</dependency>
``` 

- Export a component that implements `DiscoverableListener` with `@ExportAsService`

- Starting from JCMA v1.7.7, CCMA v3.4.0, you can also optionally export your own `PreMigrationCheckRepository` in order
  to implement pre-migration checks

## Building

Go to server app project root folder and then go to `server/discoverable-sample` folder, build the project with:

```shell
mvn clean package
```

This will produce a P2-plugin jar which can be installed into a running Confluence or Jira server using (adjust as
necessary)

```shell
atlas-install-plugin --http-port 1990 --context-path /confluence
```

# License

Copyright (c) 2023 Atlassian and others.
Apache 2.0 licensed,
see [LICENSE.txt](https://bitbucket.org/atlassianlabs/app-migration-vendor-example/src/master/LICENSE.txt) file.
