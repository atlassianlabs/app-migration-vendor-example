import { Queue } from '@forge/events';
import { storage } from '@forge/api';
import { migration } from "@forge/migrations";

const queue = new Queue({ key: 'queue-name' });

// function called when migration has started
export async function onMigrationStarted(event, context) {
    console.log('started migration');
    console.log(`\nEvent: ${JSON.stringify(event)}`);
    console.log(`\nContext: ${JSON.stringify(context)}`);

    const transferId = event.transferId;
    try {

        //Perform app migration processing. Following shows the sample invocation of each methods provided by @forge/migrations
        const responseContainers = await migration.getContainers(transferId, 'JiraProject').getMany();
        console.log(`\nContainers response is: ${JSON.stringify(responseContainers)}`);

        const responseMappingByPage = await migration.getMappings(transferId, 'jira/classic:customField').getMany();
        console.log(`\nMappings by page response is: ${JSON.stringify(responseMappingByPage)}`);
        // we can push this logic to an async queue if increased timeout is required

    } catch (e) {
        console.log(`failed to process event: ${JSON.stringify(event)} , error=${e}`);
    }
}

// function called when migration has errored
export async function onMigrationErrored(event, context) {
    console.log('Migration errored');
    console.log(`\nEvent: ${JSON.stringify(event)}`);
    console.log(`\nContext: ${JSON.stringify(context)}`);
}

// function called when app data has been uploaded
// As memory size is limited for each Forge invocation, upload small size app data file from your server app
export async function onFileUploaded(event, context) {
    console.log('file uploaded');
    console.log(`\nEvent: ${JSON.stringify(event)}`);
    console.log(`\nContext: ${JSON.stringify(context)}`);


    const transferId = event.transferId;
    const fileKey = event.key;
    try {

        //Perform app file processing. Following shows the sample invocation of each methods provided by @forge/migrations

        // get any custom field mappings
        // const responseMappingByPage = await migration.getMappings(transferId, 'jira/classic:customField').getMany();
        // console.log(`\nMappings by page response is: ${JSON.stringify(responseMappingByPage)}`);

        // get a full list of files uploaded, semi-redundant since you are given a fileKey as part of the event
        // const responseAppDataList = await migration.getAppDataList(transferId);
        // console.log(`\nApp data list response is: ${JSON.stringify(responseAppDataList)}`);

        // get the contents of the file
        const responseAppDataPayload = await migration.getAppDataPayload(fileKey)

        const appDataPayload = await responseAppDataPayload.text()
        console.log(`\nApp data payload contents is: ${JSON.stringify(appDataPayload)}`);

        // process your logic on appDataPayload, payload can be pushed onto an async queue if increased timeout is required
        // DO LOGIC HERE

        const messageId = event.messageId;
        // this call will update the status of the app migration, if it is not called within 15minutes, the file will be treated as timed out
        await migration.messageProcessed(transferId, messageId);
        console.log(`\nProcessed message`);


    } catch (e) {
        console.error(`failed to process event: ${JSON.stringify(event)} , error=${e}`);
        // Show error log to customers
        await migration.addLog(transferId, `Failed to process event: ${JSON.stringify(event)} , error=${e}`);
        // Fail the migration if we could not process the event        
        await migration.messageFailed(transferId, event.messageId);
    }
}

// function called when completeExport is called server-side
export async function onExportCompleted(event, context) {
    console.log('export completed');
    console.log(`\nEvent: ${JSON.stringify(event)}`);
    console.log(`\nContext: ${JSON.stringify(context)}`);
}

// function called when transfer has been settled.
export async function onTransferSettled(event, context) {
    console.log(`event in transferSettledHandler: ${JSON.stringify(event)}`);
    console.log(`context in transferSettledHandler: ${JSON.stringify(context)}`);
}
